package metodos;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class conectar {
    
    private Connection con;
    private static final String user ="root";
    private static final String password ="1234";
    private static final String url= "jdbc:mysql://localhost:3306/taqueria";
    
    
    public conectar(){
        try{
            con=null;
            Class.forName("com.mysql.jdbc.Driver");
            con= DriverManager.getConnection(url, user, password);
            
            if(con!=null){
                System.out.println("Conexion establecida...");
            }
            
        }catch(ClassNotFoundException | SQLException e){
            System.out.println("Error al conectar "+e);
        }
    }
    
    
    //Este metodo nos retorna la conexion.
    public Connection getConnection(){
        return con;
    }
    
    //Con este metodo nos desconectamos de la base de datos
    public void desconectar(){
        con=null;
        System.out.println("Conexion terminada..");
    }
    
    
}
