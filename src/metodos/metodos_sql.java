package metodos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class metodos_sql {
    
    public static conectar Conexion = new conectar();
    
    public static PreparedStatement sentencia_preparada;
    public static ResultSet resultado;
    public static String sql;
    public static int resultado_numero=0;
    public static metodos_sql guardarMesa;
    
    public int guardarUsuario(String nombre, String password,String telefono, String fecha, int id_rol){
        int resultadoI=0;
        Connection conexion = null;
        String sentencia_guardar= ("INSERT INTO taqueria.users (nombre, telefono, fecha_registro, password, id_rol) VALUES (?,?,?,?,?)");
        try {
            conexion = Conexion.getConnection();
            sentencia_preparada= conexion.prepareStatement(sentencia_guardar);
            sentencia_preparada.setString(1, nombre);
            sentencia_preparada.setString(2, telefono);
            sentencia_preparada.setString(3, fecha);
            sentencia_preparada.setString(4, password);
            sentencia_preparada.setInt(5, id_rol);
            resultadoI = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();
        }catch(Exception e){
            System.out.println(e);
        }
        return resultadoI;
        
    }
        public int guardarProducto(String nombreProducto,String precio){
        int resultadoI=0;
        Connection conexion = null;
        String sentencia_guardar= ("INSERT INTO taqueria.productos (nombre_producto,  costo_producto) VALUES (?,?)");
        try {
            conexion = Conexion.getConnection();
            sentencia_preparada= conexion.prepareStatement(sentencia_guardar);
            sentencia_preparada.setString(1, nombreProducto);
            sentencia_preparada.setString(2, precio);
            resultadoI = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();
        }catch(Exception e){
            System.out.println(e);
        }
        return resultadoI;
    }
    
        public int guardarMesa(String id_mesas, String capacidad){
        int resultadoI=0;
        Connection conexion = null;
        String sentencia_guardar= ("INSERT INTO taqueria.mesas (id_mesas, estado_de_mesa, capacidad) VALUES (?,?,?)");
        try {
            conexion = Conexion.getConnection();
            sentencia_preparada= conexion.prepareStatement(sentencia_guardar);
            sentencia_preparada.setString(1, id_mesas);
             sentencia_preparada.setString(2, "ocupado");
            sentencia_preparada.setString(3, capacidad);
            resultadoI = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();
        }catch(Exception e){
            System.out.println(e);
        }
        return resultadoI;

        }
        public int guardarTaqueria(String NombreSucursal,String TelefonoSucursal, String DireccionSucursal){
        int resultadoI=0;
        Connection conexion = null;
        String sentencia_guardar= ("INSERT INTO taqueria.sucursales (nombre_sucursal, telefono_sucursal,direccion_sucursal) VALUES (?,?,?)");
        try {
            conexion = Conexion.getConnection();
            sentencia_preparada= conexion.prepareStatement(sentencia_guardar);
            sentencia_preparada.setString(1, NombreSucursal);
            sentencia_preparada.setString(2, TelefonoSucursal);
            sentencia_preparada.setString(3, DireccionSucursal);
            resultadoI = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();
        }catch(Exception e){
            System.out.println(e);
        }
        return resultadoI;
        
    }

    public static String buscaNombre(String nom){
        Connection conexion =null;
        String nombre=null;
        try{
            conexion = Conexion.getConnection();
            String sentencia_buscar= ("SELECT nombre FROM taqueria.users WHERE nombre= '"+nom+"'");
            sentencia_preparada=conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            
            if(resultado.next()){
                nombre= resultado.getString("nombre");
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return nombre;
    }
        public static String buscaNombreProducto(String nomP){
        Connection conexion =null;
        String nombre=null;
        try{
            conexion = Conexion.getConnection();
            String sentencia_buscar= ("SELECT nombre_producto FROM taqueria.productos WHERE nombre_producto = '"+nomP+"'");
            sentencia_preparada=conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            
            if(resultado.next()){
                nombre= resultado.getString("nombre_producto");
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return nombre;
    }
        public static String buscaNombreMesa(String idM){
        Connection conexion =null;
        String nombreM=null;
        try{
            conexion = Conexion.getConnection();
            String sentencia_buscar= ("SELECT id_mesas FROM taqueria.mesas WHERE id_mesas= '"+idM+"'");
            sentencia_preparada=conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            
            if(resultado.next()){
                idM= resultado.getString("id_mesas");
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return idM;
    }
    public static String buscaNombreSucursales(String nomT){
        Connection conexion =null;
        String nombreT=null;
        try{
            conexion = Conexion.getConnection();
            String sentencia_buscar= ("SELECT nombre_sucursal FROM taqueria.sucursales WHERE nombre_sucursal= '"+nomT+"'");
            sentencia_preparada=conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            
            if(resultado.next()){
                nomT= resultado.getString("nombre_sucursal");
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return nomT;
    }

    
    public static String buscarUser(String nom, String pass){
        String busqueda_usuario=null;
        Connection conexion=null;
        try{
            conexion=Conexion.getConnection();
            String sentencia_buscar_usuario=("SELECT nombre,password FROM taqueria.users WHERE nombre= '"+nom+"' && password= '"+pass+"'");
            sentencia_preparada=conexion.prepareStatement(sentencia_buscar_usuario);
            resultado=sentencia_preparada.executeQuery();
            if(resultado.next()){
                busqueda_usuario="Usuario encontrado";
            }
            else{
                busqueda_usuario="Usuario no encontrado";
            }
        }catch(Exception e){
           System.out.println(e);
        }
        
        return busqueda_usuario;
    }
    
        public static boolean buscarUserRegistrado(String nom, String telefono){
        boolean busqueda_usuario=false;
        Connection conexion=null;
        try{
            conexion=Conexion.getConnection();
            String sentencia_buscar_usuario=("SELECT nombre,telefono FROM taqueria.users WHERE nombre= '"+nom+"' OR telefono= '"+telefono+"'");
            sentencia_preparada=conexion.prepareStatement(sentencia_buscar_usuario);
            resultado=sentencia_preparada.executeQuery();
            if(resultado.next()){
                busqueda_usuario = true;
            }
            else{
                busqueda_usuario=false;
            }
        }catch(Exception e){
           System.out.println(e);
        }
        
        return busqueda_usuario;
    }
        
        public static String buscarProducto(String nomP){
        String busqueda_producto=null;
        Connection conexion=null;
        try{
            conexion=Conexion.getConnection();
            String sentencia_buscar_producto=("SELECT nombre_producto FROM taqueria.productos WHERE nombre_producto= '"+nomP+"'");
            sentencia_preparada=conexion.prepareStatement(sentencia_buscar_producto);
            resultado=sentencia_preparada.executeQuery();
            if(resultado.next()){
                busqueda_producto="Producto encontrado";
            }
            else{
                busqueda_producto="Producto no encontrado";
            }
        }catch(Exception e){
           System.out.println(e);
        }
        
        return busqueda_producto;
    }
    public boolean buscarProducRegistrado(String nomP){
        boolean busqueda_producto=false;
        Connection conexion=null;
        try{
            conexion=Conexion.getConnection();
            String sentencia_buscar_producto=("SELECT nombre_producto FROM taqueria.productos WHERE nombre_producto= '"+nomP+"'");
            sentencia_preparada=conexion.prepareStatement(sentencia_buscar_producto);
            resultado=sentencia_preparada.executeQuery();
            if(resultado.next()){
                busqueda_producto = true;
            }
            else{
                busqueda_producto=false;
            }
        }catch(Exception e){
           System.out.println(e);
        }
        
        return busqueda_producto;
    }
            public static String buscarMesas(String IDMesas){
        String busqueda_Mesas=null;
        Connection conexion=null;
        try{
            conexion=Conexion.getConnection();
            String sentencia_buscar_Mesas=("SELECT id_mesas FROM taqueria.mesas WHERE id_mesas= '"+IDMesas+"'");
            sentencia_preparada=conexion.prepareStatement(sentencia_buscar_Mesas);
            resultado=sentencia_preparada.executeQuery();
            if(resultado.next()){
                busqueda_Mesas="Mesa encontrada";
            }
            else{
                busqueda_Mesas ="Mesa no encontrada";
            }
        }catch(Exception e){
           System.out.println(e);
        }
        
        return busqueda_Mesas;
    }
    public static boolean buscarMesasRegistradas(String IDMesas){
        boolean busqueda_Mesas=false;
        Connection conexion=null;
        try{
            conexion=Conexion.getConnection();
            String sentencia_buscar_Mesas=("SELECT id_mesas FROM taqueria.mesas WHERE id_mesas= '"+IDMesas+"'");
            sentencia_preparada=conexion.prepareStatement(sentencia_buscar_Mesas);
            resultado=sentencia_preparada.executeQuery();
            if(resultado.next()){
                busqueda_Mesas = true;
            }
            else{
                busqueda_Mesas=false;
            }
        }catch(Exception e){
           System.out.println(e);
        }
        
        return busqueda_Mesas;
    }

            public static String buscarSucursal(String nomSuc, String IDSuc){
        String busqueda_Sucursal=null;
        Connection conexion=null;
        try{
            conexion=Conexion.getConnection();
            String sentencia_buscar_Sucursal=("SELECT nombre_sucursal, id_sucursales FROM taqueria.sucursales WHERE  nombre_sucursal = '"+nomSuc+ "' && id_sucursales = '"+IDSuc+"'");
            sentencia_preparada=conexion.prepareStatement(sentencia_buscar_Sucursal);
            resultado=sentencia_preparada.executeQuery();
            if(resultado.next()){
                busqueda_Sucursal="Sucursal encontrada";
            }
            else{
                busqueda_Sucursal ="Sucursal no encontrada";
            }
        }catch(Exception e){
           System.out.println(e);
        }
        
        return busqueda_Sucursal;
    }
    public static boolean buscarSucursalRegistrada(String nomSuc, String IDSuc){
        boolean busqueda_Sucursal=false;
        Connection conexion=null;
        try{
            conexion=Conexion.getConnection();
            String sentencia_buscar_Sucursal=("SELECT nombre_sucursal,id_sucursales FROM taqueria.sucursales WHERE nombre_sucursal = '"+nomSuc+ "' && id_sucursales= '"+IDSuc+"'");
            sentencia_preparada=conexion.prepareStatement(sentencia_buscar_Sucursal);
            resultado=sentencia_preparada.executeQuery();
            if(resultado.next()){
                busqueda_Sucursal = true;
            }
            else{
                busqueda_Sucursal=false;
            }
        }catch(Exception e){
           System.out.println(e);
        }
        
        return busqueda_Sucursal;
    }
    
        public static String buscarOrden(String noOrden, String noUser){
        String busqueda_Orden=null;
        Connection conexion=null;
        try{
            conexion=Conexion.getConnection();
            String sentencia_buscar_Orden=("SELECT id_venta,id_usuario FROM taqueria.detalle_venta WHERE id_venta= '"+noOrden+"' && id_usuario= '"+noUser+"'");
            sentencia_preparada=conexion.prepareStatement(sentencia_buscar_Orden);
            resultado=sentencia_preparada.executeQuery();
            if(resultado.next()){
                busqueda_Orden="Orden encontrada";
            }
            else{
                busqueda_Orden="Orden no encontrada";
            }
        }catch(Exception e){
           System.out.println(e);
        }
        
        return busqueda_Orden;
    }
    
        public static boolean buscarOrdenRegistrado(String noOrden, String noUsuario){
        boolean busqueda_Orden=false;
        Connection conexion=null;
        try{
            conexion=Conexion.getConnection();
            String sentencia_buscar_Orden=("SELECT id_venta,id_usuario FROM taqueria.detalle_venta WHERE id_venta= '"+noOrden+"' OR id_usuario= '"+noUsuario+"'");
            sentencia_preparada=conexion.prepareStatement(sentencia_buscar_Orden);
            resultado=sentencia_preparada.executeQuery();
            if(resultado.next()){
                busqueda_Orden = true;
            }
            else{
                busqueda_Orden=false;
            }
        }catch(Exception e){
           System.out.println(e);
        }
        
        return busqueda_Orden;
    }
    
    public boolean eliminarUsuario(String id_user){
          String eliminar=null;
        Connection conexion=null;
        boolean a=false;
        try{
            conexion=Conexion.getConnection();
            eliminar=("DELETE FROM taqueria.users WHERE id_usuario="+id_user);
              sentencia_preparada=conexion.prepareStatement(eliminar);
              int b =  sentencia_preparada.executeUpdate();
            if(b==1){
                a=true;
            }
            else{
                a=false;
            }
        }catch(Exception e){
           System.out.println(e);
        }

        return a;
    }
    
    public boolean eliminarProductos(String id_productos){
          String eliminar=null;
        Connection conexion=null;
        boolean a=false;
        try{
            conexion=Conexion.getConnection();
            eliminar=("DELETE FROM taqueria.productos WHERE id_productos="+id_productos);
              sentencia_preparada=conexion.prepareStatement(eliminar);
              int b =  sentencia_preparada.executeUpdate();
            if(b==1){
                a=true;
            }
            else{
                a=false;
            }
        }catch(Exception e){
           System.out.println(e);
        }

        return a;
    }

    public boolean eliminarMesas(String id_mesas){
          String eliminar=null;
        Connection conexion=null;
        boolean a=false;
        try{
            conexion=Conexion.getConnection();
            eliminar=("DELETE FROM taqueria.mesas WHERE id_mesas="+id_mesas);
              sentencia_preparada=conexion.prepareStatement(eliminar);
              int b =  sentencia_preparada.executeUpdate();
            if(b==1){
                a=true;
            }
            else{
                a=false;
            }
        }catch(Exception e){
           System.out.println(e);
        }

        return a;
    }
    
    public boolean eliminarTaqueria(String id_sucursales){
          String eliminar=null;
        Connection conexion=null;
        boolean a=false;
        try{
            conexion=Conexion.getConnection();
            eliminar=("DELETE FROM taqueria.sucursales WHERE id_sucursales="+id_sucursales);
              sentencia_preparada=conexion.prepareStatement(eliminar);
              int b =  sentencia_preparada.executeUpdate();
            if(b==1){
                a=true;
            }
            else{
                a=false;
            }
        }catch(Exception e){
           System.out.println(e);
        }

        return a;
    }

    public boolean editarUsuario(String id, String nombre, String password, String celular, int rol){
        String actualizar=null;
        Connection conexion=null;
        int id_rol=rol+1;
        boolean a=false;
        
        try{
            conexion=Conexion.getConnection();
            actualizar=("UPDATE taqueria.users SET nombre='"+nombre+"', telefono='"+celular+"', password='"+password+"', id_rol="+id_rol+" WHERE id_usuario="+id);
              sentencia_preparada=conexion.prepareStatement(actualizar);
              int b =  sentencia_preparada.executeUpdate();
            if(b==1){
                a=true;
            }
            else{
                a=false;
            }
        }catch(Exception e){
           System.out.println(e);
        }
       
        return true;
    }
    
        public boolean editarProductos(String id, String nombre_producto, String costo_producto){
        String actualizar=null;
        Connection conexion=null;
        boolean a=false;
        
        try{
            conexion=Conexion.getConnection();
            actualizar=("UPDATE taqueria.productos SET nombre_producto='"+nombre_producto+"', costo_producto='"+costo_producto+"' WHERE id_productos ="+id);
              sentencia_preparada=conexion.prepareStatement(actualizar);
              int b =  sentencia_preparada.executeUpdate();
            if(b==1){
                a=true;
            }
            else{
                a=false;
            }
        }catch(Exception e){
           System.out.println(e);
        }
       
        return true;
    }
        
    public boolean editarMesa(String id_mesas, String capacidad){
        String actualizar=null;
        Connection conexion=null;
        boolean a=false;
        
        try{
            conexion=Conexion.getConnection();
            actualizar=("UPDATE taqueria.mesas SET id_mesas='"+id_mesas+"', capacidad='"+capacidad+"'WHERE id_mesas='"+id_mesas);
              sentencia_preparada=conexion.prepareStatement(actualizar);
              int b =  sentencia_preparada.executeUpdate();
            if(b==1){
                a=true;
            }
            else{
                a=false;
            }
        }catch(Exception e){
           System.out.println(e);
        }
       
        return true;
    }
    
    public boolean editarTaqueria(String nombre_sucursal, String telefono_sucursal, String direccion_sucursal){
        String actualizar=null;
        Connection conexion=null;
        boolean a=false;
        
        try{
            conexion=Conexion.getConnection();
            actualizar=("UPDATE taqueria.sucursales SET nombre_sucursal='"+nombre_sucursal+"', telefono_sucursal='"+telefono_sucursal+"' direccion_sucursal='"+direccion_sucursal+"'WHERE nombre_sucursal ='"+nombre_sucursal+"'");
              sentencia_preparada=conexion.prepareStatement(actualizar);
              int b =  sentencia_preparada.executeUpdate();
            if(b==1){
                a=true;
            }
            else{
                a=false;
            }
        }catch(Exception e){
           System.out.println(e);
        }
       
        return true;
    }


}