import java.sql.Connection;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import metodos.metodos_sql;
import static metodos.metodos_sql.Conexion;
import static metodos.metodos_sql.resultado;
import static metodos.metodos_sql.sentencia_preparada;

public class MenuTaqueria extends javax.swing.JFrame {

    public MenuTaqueria() {
        initComponents();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        LabelMenuTaquerias = new javax.swing.JLabel();
        BotonAñadirTaqueria = new javax.swing.JButton();
        LabelTaqueriasFuncionando = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        BotonMostrarTaquerias = new javax.swing.JButton();
        BotonSalirTaqueria = new javax.swing.JButton();
        BotonModificarTaquerias = new javax.swing.JButton();
        BotonEliminarTaqueria = new javax.swing.JButton();
        scrollPane1 = new java.awt.ScrollPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        TablaTaqueria = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(198, 235, 249));

        LabelMenuTaquerias.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LabelMenuTaquerias.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/LOGO.png"))); // NOI18N
        LabelMenuTaquerias.setText("Menu de Taquerías");

        BotonAñadirTaqueria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/añadir.png"))); // NOI18N
        BotonAñadirTaqueria.setText("Añadir nueva Taquería");
        BotonAñadirTaqueria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonAñadirTaqueriaActionPerformed(evt);
            }
        });

        LabelTaqueriasFuncionando.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        LabelTaqueriasFuncionando.setText("Taquerías en funcionamiento");

        jPanel2.setBackground(new java.awt.Color(173, 245, 191));

        BotonMostrarTaquerias.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/BUSCAR.png"))); // NOI18N
        BotonMostrarTaquerias.setText("Mostrar datos ");
        BotonMostrarTaquerias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonMostrarTaqueriasActionPerformed(evt);
            }
        });

        BotonSalirTaqueria.setBackground(new java.awt.Color(173, 245, 191));
        BotonSalirTaqueria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/2.png"))); // NOI18N
        BotonSalirTaqueria.setText("Salir");
        BotonSalirTaqueria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonSalirTaqueriaActionPerformed(evt);
            }
        });

        BotonModificarTaquerias.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ModificarDatos.png"))); // NOI18N
        BotonModificarTaquerias.setText("Modificar datos ");
        BotonModificarTaquerias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonModificarTaqueriasActionPerformed(evt);
            }
        });

        BotonEliminarTaqueria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Limpiar.png"))); // NOI18N
        BotonEliminarTaqueria.setText("Eliminar taquería");
        BotonEliminarTaqueria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonEliminarTaqueriaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(78, Short.MAX_VALUE)
                .addComponent(BotonMostrarTaquerias)
                .addGap(18, 18, 18)
                .addComponent(BotonModificarTaquerias)
                .addGap(36, 36, 36)
                .addComponent(BotonEliminarTaqueria)
                .addGap(42, 42, 42)
                .addComponent(BotonSalirTaqueria, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(55, 55, 55))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(22, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BotonMostrarTaquerias)
                    .addComponent(BotonModificarTaquerias)
                    .addComponent(BotonEliminarTaqueria)
                    .addComponent(BotonSalirTaqueria))
                .addContainerGap())
        );

        scrollPane1.setBackground(new java.awt.Color(255, 255, 255));

        TablaTaqueria.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(TablaTaqueria);

        scrollPane1.add(jScrollPane1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(BotonAñadirTaqueria))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addComponent(scrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 632, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(242, 242, 242)
                        .addComponent(LabelTaqueriasFuncionando)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LabelMenuTaquerias)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(LabelMenuTaquerias)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BotonAñadirTaqueria)
                .addGap(18, 18, 18)
                .addComponent(LabelTaqueriasFuncionando)
                .addGap(20, 20, 20)
                .addComponent(scrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 71, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BotonSalirTaqueriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonSalirTaqueriaActionPerformed
        MenuPrincipal MP=new MenuPrincipal();
        MP.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonSalirTaqueriaActionPerformed

    private void BotonAñadirTaqueriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonAñadirTaqueriaActionPerformed
        AñadirTaqueríaNueva AT=new AñadirTaqueríaNueva();
        AT.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonAñadirTaqueriaActionPerformed

    private void BotonMostrarTaqueriasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonMostrarTaqueriasActionPerformed
        getAllSucursales();
    }//GEN-LAST:event_BotonMostrarTaqueriasActionPerformed

    private void BotonEliminarTaqueriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonEliminarTaqueriaActionPerformed
        int res= JOptionPane.showConfirmDialog(null, "¿Seguro que deseas eliminar?");
       if(res==0){
        metodos_sql metodos= new metodos_sql();
        if (TablaTaqueria.getSelectedRow()==-1){
            JOptionPane.showMessageDialog(null, "Seleccione una Taqueria primero");
        }else {
        if(metodos.eliminarTaqueria( TablaTaqueria.getValueAt(TablaTaqueria.getSelectedRow(), 0).toString())){
            System.out.println("Eliminado correctamente");
           BotonMostrarTaquerias.doClick();
        }
        else{
            JOptionPane.showMessageDialog(null, "Error al eliminar");
        }
        }
       }
       else {

       }
    }//GEN-LAST:event_BotonEliminarTaqueriaActionPerformed

    private void BotonModificarTaqueriasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonModificarTaqueriasActionPerformed
        ModificarTaqueria MT=new ModificarTaqueria();
        MT.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonModificarTaqueriasActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MenuTaqueria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MenuTaqueria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MenuTaqueria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuTaqueria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MenuTaqueria().setVisible(true);
            }
        });
    }
    
     public void getAllSucursales(){
        Connection conexion=null;
        String[] datos = new String[4];
        DefaultTableModel modelo = new DefaultTableModel();
        try {
        conexion= Conexion.getConnection();
        System.err.println("me conecte");
        String sentencia_getAllSucursales=("SELECT * FROM taqueria.sucursales");
        sentencia_preparada=conexion.prepareStatement(sentencia_getAllSucursales);
        resultado=sentencia_preparada.executeQuery();
            modelo.addColumn("id_sucursales");
            modelo.addColumn("nombre_sucursal");
            modelo.addColumn("telefono_sucursal");
            modelo.addColumn("direccion_sucursal");
            TablaTaqueria.setModel(modelo);
        while(resultado.next()){
            datos[0]=resultado.getString(1);
            datos[1]=resultado.getString(2);
            datos[2]=resultado.getString(3);
            datos[3]=resultado.getString(4);
            modelo.addRow(datos);
           TablaTaqueria.setModel(modelo);
           /*System.out.println(datos[0]);
           System.out.println(datos[1]);
           System.out.println(datos[2]);*/
        }
        System.out.println("inserte los datos");
        }catch(Exception e){
            System.out.println(e);
        }
     }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotonAñadirTaqueria;
    private javax.swing.JButton BotonEliminarTaqueria;
    private javax.swing.JButton BotonModificarTaquerias;
    private javax.swing.JButton BotonMostrarTaquerias;
    private javax.swing.JButton BotonSalirTaqueria;
    private javax.swing.JLabel LabelMenuTaquerias;
    private javax.swing.JLabel LabelTaqueriasFuncionando;
    private javax.swing.JTable TablaTaqueria;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private java.awt.ScrollPane scrollPane1;
    // End of variables declaration//GEN-END:variables
}
