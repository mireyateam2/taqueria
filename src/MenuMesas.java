import java.sql.Connection;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import metodos.metodos_sql;
import static metodos.metodos_sql.Conexion;
import static metodos.metodos_sql.resultado;
import static metodos.metodos_sql.sentencia_preparada;

public class MenuMesas extends javax.swing.JFrame {
public String id;
    public MenuMesas() {
        initComponents();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        LabelMenuMesa = new javax.swing.JLabel();
        BotonAñadirMesa = new javax.swing.JButton();
        LabelMesasDisponibles = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        BotonEliminarMesa = new javax.swing.JButton();
        BotonModificarMesa = new javax.swing.JButton();
        BotonMostrarMesas = new javax.swing.JButton();
        BotonSalirMesas = new javax.swing.JButton();
        scrollPane1 = new java.awt.ScrollPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        TablaMesa = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(198, 235, 249));

        LabelMenuMesa.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        LabelMenuMesa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/LOGO.png"))); // NOI18N
        LabelMenuMesa.setText("Menu de mesa");

        BotonAñadirMesa.setBackground(new java.awt.Color(173, 245, 191));
        BotonAñadirMesa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/añadir.png"))); // NOI18N
        BotonAñadirMesa.setText("Añadir nueva mesa");
        BotonAñadirMesa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonAñadirMesaActionPerformed(evt);
            }
        });

        LabelMesasDisponibles.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        LabelMesasDisponibles.setText("Mesas disponibles");

        jPanel2.setBackground(new java.awt.Color(173, 245, 191));

        BotonEliminarMesa.setBackground(new java.awt.Color(173, 245, 191));
        BotonEliminarMesa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Limpiar.png"))); // NOI18N
        BotonEliminarMesa.setText("Eliminar mesa");
        BotonEliminarMesa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonEliminarMesaActionPerformed(evt);
            }
        });

        BotonModificarMesa.setBackground(new java.awt.Color(173, 245, 191));
        BotonModificarMesa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/MESAS.png"))); // NOI18N
        BotonModificarMesa.setText("Modificar mesa en servicio");
        BotonModificarMesa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonModificarMesaActionPerformed(evt);
            }
        });

        BotonMostrarMesas.setBackground(new java.awt.Color(173, 245, 191));
        BotonMostrarMesas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/MESA.png"))); // NOI18N
        BotonMostrarMesas.setText("Mostrar mesas");
        BotonMostrarMesas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonMostrarMesasActionPerformed(evt);
            }
        });

        BotonSalirMesas.setBackground(new java.awt.Color(173, 245, 191));
        BotonSalirMesas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/2.png"))); // NOI18N
        BotonSalirMesas.setText("Salir");
        BotonSalirMesas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonSalirMesasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BotonEliminarMesa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BotonModificarMesa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BotonMostrarMesas)
                .addGap(18, 18, 18)
                .addComponent(BotonSalirMesas)
                .addGap(58, 58, 58))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BotonEliminarMesa)
                    .addComponent(BotonModificarMesa)
                    .addComponent(BotonSalirMesas)
                    .addComponent(BotonMostrarMesas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        scrollPane1.setBackground(new java.awt.Color(255, 255, 255));

        TablaMesa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(TablaMesa);

        scrollPane1.add(jScrollPane1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(LabelMenuMesa)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(BotonAñadirMesa))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(222, 222, 222)
                        .addComponent(LabelMesasDisponibles))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(scrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 632, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(11, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(LabelMenuMesa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BotonAñadirMesa, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(LabelMesasDisponibles)
                .addGap(2, 2, 2)
                .addComponent(scrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BotonAñadirMesaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonAñadirMesaActionPerformed
        AñadirMesaNueva AM=new AñadirMesaNueva();
        AM.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonAñadirMesaActionPerformed

    private void BotonEliminarMesaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonEliminarMesaActionPerformed
        int res= JOptionPane.showConfirmDialog(null, "¿Seguro que deseas eliminar?");
       if(res==0){
        metodos_sql metodos= new metodos_sql();
        if (TablaMesa.getSelectedRow()==-1){
            JOptionPane.showMessageDialog(null, "Seleccione una mesa primero");
        }else {
        if(metodos.eliminarMesas( TablaMesa.getValueAt(TablaMesa.getSelectedRow(), 0).toString())){
            System.out.println("Eliminado correctamente");
           BotonMostrarMesas.doClick();
        }
        else{
            JOptionPane.showMessageDialog(null, "Error al eliminar");
        }
        }
       }
       else {

       }
    }//GEN-LAST:event_BotonEliminarMesaActionPerformed

    private void BotonSalirMesasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonSalirMesasActionPerformed
        MenuPrincipal MP=new MenuPrincipal();
        MP.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonSalirMesasActionPerformed

    private void BotonMostrarMesasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonMostrarMesasActionPerformed
        getAllMesas();
    }//GEN-LAST:event_BotonMostrarMesasActionPerformed

    private void BotonModificarMesaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonModificarMesaActionPerformed
        id = TablaMesa.getValueAt(TablaMesa.getSelectedRow(),0).toString();
        ModificarMesas MM=new ModificarMesas(id);
        MM.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonModificarMesaActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MenuMesas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MenuMesas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MenuMesas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuMesas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MenuMesas().setVisible(true);
            }
        });
    }
    
      public void getAllMesas(){
        Connection conexion=null;
        String[] datos = new String[3];
        DefaultTableModel modelo = new DefaultTableModel();
        try {
        conexion= Conexion.getConnection();
        System.err.println("me conecte");
        String sentencia_getAllMesas=("SELECT * FROM taqueria.mesas");
        sentencia_preparada=conexion.prepareStatement(sentencia_getAllMesas);
        resultado=sentencia_preparada.executeQuery();
            modelo.addColumn("id_mesas");
            modelo.addColumn("estado_de_mesas");
            modelo.addColumn("capacidad");
            TablaMesa.setModel(modelo);
        while(resultado.next()){
            datos[0]=resultado.getString(1);
            datos[1]=resultado.getString(2);
            datos[2]=resultado.getString(3);
            modelo.addRow(datos);
           TablaMesa.setModel(modelo);
           /*System.out.println(datos[0]);
           System.out.println(datos[1]);
           System.out.println(datos[2]);*/
        }
        System.out.println("inserte los datos");
        }catch(Exception e){
            System.out.println(e);
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotonAñadirMesa;
    private javax.swing.JButton BotonEliminarMesa;
    private javax.swing.JButton BotonModificarMesa;
    private javax.swing.JButton BotonMostrarMesas;
    private javax.swing.JButton BotonSalirMesas;
    private javax.swing.JLabel LabelMenuMesa;
    private javax.swing.JLabel LabelMesasDisponibles;
    private javax.swing.JTable TablaMesa;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private java.awt.ScrollPane scrollPane1;
    // End of variables declaration//GEN-END:variables
}
