public class MenuPrincipal extends javax.swing.JFrame {

    public MenuPrincipal() {
        initComponents();
        setLocationRelativeTo(null);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        LabelMenuPrincipal = new javax.swing.JLabel();
        BotonMenuEmpleados = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        BotonSalirMenuPrin = new javax.swing.JButton();
        BotonMenuAdministrador = new javax.swing.JButton();
        BotonMenuProductos = new javax.swing.JButton();
        BotonDatosdeVenta = new javax.swing.JButton();
        BotonEliminarVentas = new javax.swing.JButton();
        BotonMenuMesas = new javax.swing.JButton();
        BotonConsultarVentas = new javax.swing.JButton();
        BotonMenuTaqueria = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBackground(new java.awt.Color(198, 235, 249));

        LabelMenuPrincipal.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LabelMenuPrincipal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/LOGO.png"))); // NOI18N
        LabelMenuPrincipal.setText("MENU PRINCIPAL");

        BotonMenuEmpleados.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EMPLEADOS.png"))); // NOI18N
        BotonMenuEmpleados.setText("MENU EMPLEADOS");
        BotonMenuEmpleados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonMenuEmpleadosActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(173, 245, 191));

        BotonSalirMenuPrin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/volver.png"))); // NOI18N
        BotonSalirMenuPrin.setText("Volver");
        BotonSalirMenuPrin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonSalirMenuPrinActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(573, Short.MAX_VALUE)
                .addComponent(BotonSalirMenuPrin, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(288, 288, 288))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(BotonSalirMenuPrin)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        BotonMenuAdministrador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ADMINISTRADOR.png"))); // NOI18N
        BotonMenuAdministrador.setText("MENU ADMINISTRADOR");
        BotonMenuAdministrador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonMenuAdministradorActionPerformed(evt);
            }
        });

        BotonMenuProductos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/TACOS.png"))); // NOI18N
        BotonMenuProductos.setText("MENU PRODUCTO");
        BotonMenuProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonMenuProductosActionPerformed(evt);
            }
        });

        BotonDatosdeVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/VENTAS.png"))); // NOI18N
        BotonDatosdeVenta.setText("DATOS DE LA VENTA");
        BotonDatosdeVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonDatosdeVentaActionPerformed(evt);
            }
        });

        BotonEliminarVentas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ELIMINARV.png"))); // NOI18N
        BotonEliminarVentas.setText("ELIMINAR VENTAS");
        BotonEliminarVentas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonEliminarVentasActionPerformed(evt);
            }
        });

        BotonMenuMesas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/MESAS (1).png"))); // NOI18N
        BotonMenuMesas.setText("MENU DE MESAS");
        BotonMenuMesas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonMenuMesasActionPerformed(evt);
            }
        });

        BotonConsultarVentas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Consultar.png"))); // NOI18N
        BotonConsultarVentas.setText("CONSULTA DE VENTAS");
        BotonConsultarVentas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonConsultarVentasActionPerformed(evt);
            }
        });

        BotonMenuTaqueria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/TAQUERIAA.png"))); // NOI18N
        BotonMenuTaqueria.setText("MENU DE TAQUERÍAS");
        BotonMenuTaqueria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonMenuTaqueriaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(168, 168, 168)
                .addComponent(BotonMenuAdministrador, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(89, 89, 89)
                .addComponent(BotonMenuEmpleados, javax.swing.GroupLayout.PREFERRED_SIZE, 288, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(BotonDatosdeVenta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(BotonMenuProductos, javax.swing.GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE))
                                .addGap(30, 30, 30)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(BotonConsultarVentas, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(BotonMenuMesas, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(BotonMenuTaqueria, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
                                    .addComponent(BotonEliminarVentas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(LabelMenuPrincipal))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(LabelMenuPrincipal)
                .addGap(2, 2, 2)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BotonMenuAdministrador)
                    .addComponent(BotonMenuEmpleados, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(BotonMenuProductos, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(BotonMenuTaqueria, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(BotonMenuMesas, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BotonConsultarVentas, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotonEliminarVentas)
                    .addComponent(BotonDatosdeVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 1015, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BotonMenuEmpleadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonMenuEmpleadosActionPerformed
        MenuUsuario MU= new MenuUsuario();
        MU.show (true);
        this.dispose();
    }//GEN-LAST:event_BotonMenuEmpleadosActionPerformed

    private void BotonMenuAdministradorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonMenuAdministradorActionPerformed
        MenuUsuariosAdmi MA= new MenuUsuariosAdmi();
        MA.show (true);
        this.dispose();
    }//GEN-LAST:event_BotonMenuAdministradorActionPerformed

    private void BotonMenuProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonMenuProductosActionPerformed
        MenuProductos MP= new MenuProductos();
        MP.show (true);
        this.dispose();
    }//GEN-LAST:event_BotonMenuProductosActionPerformed

    private void BotonMenuMesasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonMenuMesasActionPerformed
        MenuMesas Mm= new MenuMesas();
        Mm.show (true);
        this.dispose();
    }//GEN-LAST:event_BotonMenuMesasActionPerformed

    private void BotonMenuTaqueriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonMenuTaqueriaActionPerformed
        MenuTaqueria Mt= new MenuTaqueria();
        Mt.show (true);
        this.dispose();
    }//GEN-LAST:event_BotonMenuTaqueriaActionPerformed

    private void BotonDatosdeVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonDatosdeVentaActionPerformed
        RegistroDeVentas DV=new RegistroDeVentas();
        DV.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonDatosdeVentaActionPerformed

    private void BotonConsultarVentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonConsultarVentasActionPerformed
        ConsultaDeVentas CV=new ConsultaDeVentas();
        CV.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonConsultarVentasActionPerformed

    private void BotonEliminarVentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonEliminarVentasActionPerformed
        Eliminiarventas EV=new Eliminiarventas();
        EV.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonEliminarVentasActionPerformed

    private void BotonSalirMenuPrinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonSalirMenuPrinActionPerformed
        IniciodeSesion IC=new IniciodeSesion();
        IC.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonSalirMenuPrinActionPerformed
    

    @Override
    public void doLayout() {
        super.doLayout(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void disable() {
        super.disable(); //To change body of generated methods, choose Tools | Templates.
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MenuPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotonConsultarVentas;
    private javax.swing.JButton BotonDatosdeVenta;
    private javax.swing.JButton BotonEliminarVentas;
    private javax.swing.JButton BotonMenuAdministrador;
    private javax.swing.JButton BotonMenuEmpleados;
    private javax.swing.JButton BotonMenuMesas;
    private javax.swing.JButton BotonMenuProductos;
    private javax.swing.JButton BotonMenuTaqueria;
    private javax.swing.JButton BotonSalirMenuPrin;
    private javax.swing.JLabel LabelMenuPrincipal;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    // End of variables declaration//GEN-END:variables
}
