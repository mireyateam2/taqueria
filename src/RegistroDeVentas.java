import java.sql.Connection;
import java.util.Date;
import javax.swing.table.DefaultTableModel;
import static metodos.metodos_sql.Conexion;
import static metodos.metodos_sql.resultado;
import static metodos.metodos_sql.sentencia_preparada;
public class RegistroDeVentas extends javax.swing.JFrame {

    public RegistroDeVentas() {
        initComponents();
        setLocationRelativeTo(null);
        Date fecha=new Date();
        FechaOrden.enable(false);
        String f = fecha.toGMTString().substring(0, 12);
        FechaOrden.setText(f);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        LabelDatosdelaVenta = new javax.swing.JLabel();
        BotonTicket = new javax.swing.JButton();
        RegistrodeVentasLabel = new javax.swing.JLabel();
        BotonSalirOrden = new javax.swing.JButton();
        BotonAgregarOrden = new javax.swing.JButton();
        jScrollPane8 = new javax.swing.JScrollPane();
        FechaOrden = new javax.swing.JTextPane();
        FechaOrdenLabel = new javax.swing.JLabel();
        scrollPane1 = new java.awt.ScrollPane();
        jScrollPane3 = new javax.swing.JScrollPane();
        OrdenTabla = new javax.swing.JTable();
        MostrarOrden = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(java.awt.Color.green);
        setName("Ventas"); // NOI18N

        jPanel3.setBackground(new java.awt.Color(198, 235, 249));

        LabelDatosdelaVenta.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LabelDatosdelaVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/LOGO.png"))); // NOI18N
        LabelDatosdelaVenta.setText("Datos de la venta");

        BotonTicket.setBackground(new java.awt.Color(173, 245, 191));
        BotonTicket.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/TICKET.png"))); // NOI18N
        BotonTicket.setText("Imprimir ticket");

        RegistrodeVentasLabel.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        RegistrodeVentasLabel.setText("Registro de ventas");

        BotonSalirOrden.setBackground(new java.awt.Color(173, 245, 191));
        BotonSalirOrden.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/2.png"))); // NOI18N
        BotonSalirOrden.setText("Salir");
        BotonSalirOrden.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonSalirOrdenActionPerformed(evt);
            }
        });

        BotonAgregarOrden.setBackground(new java.awt.Color(173, 245, 191));
        BotonAgregarOrden.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/añadir.png"))); // NOI18N
        BotonAgregarOrden.setText("Agregar");
        BotonAgregarOrden.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonAgregarOrdenActionPerformed(evt);
            }
        });

        FechaOrden.setEnabled(false);
        jScrollPane8.setViewportView(FechaOrden);

        FechaOrdenLabel.setText("Fecha");

        scrollPane1.setBackground(new java.awt.Color(255, 255, 255));

        OrdenTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(OrdenTabla);

        scrollPane1.add(jScrollPane3);

        MostrarOrden.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/BUSCAR.png"))); // NOI18N
        MostrarOrden.setText("Mostrar productos");
        MostrarOrden.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MostrarOrdenActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(175, 175, 175)
                        .addComponent(RegistrodeVentasLabel))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(LabelDatosdelaVenta)
                                .addGroup(jPanel3Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(BotonAgregarOrden)))
                            .addGap(182, 182, 182)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(FechaOrdenLabel)))
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(scrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                            .addGap(23, 23, 23)
                            .addComponent(BotonTicket)
                            .addGap(28, 28, 28)
                            .addComponent(MostrarOrden)
                            .addGap(36, 36, 36)
                            .addComponent(BotonSalirOrden))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelDatosdelaVenta)
                    .addComponent(FechaOrdenLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotonAgregarOrden))
                .addGap(8, 8, 8)
                .addComponent(RegistrodeVentasLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BotonSalirOrden)
                    .addComponent(BotonTicket)
                    .addComponent(MostrarOrden))
                .addContainerGap(32, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BotonSalirOrdenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonSalirOrdenActionPerformed
        MenuPrincipal MP=new MenuPrincipal();
        MP.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonSalirOrdenActionPerformed

    private void BotonAgregarOrdenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonAgregarOrdenActionPerformed
        AñadirOrden AO=new AñadirOrden();
        AO.show(true);
        this.dispose();
        
    }//GEN-LAST:event_BotonAgregarOrdenActionPerformed

    private void MostrarOrdenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MostrarOrdenActionPerformed
        getAllOrdenTabla();
    }//GEN-LAST:event_MostrarOrdenActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistroDeVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistroDeVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistroDeVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistroDeVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegistroDeVentas().setVisible(true);
            }
        });
    }
    
    public void getAllOrdenTabla(){
        Connection conexion=null;
        String[] datos = new String[6];
        DefaultTableModel modelo = new DefaultTableModel();
        try {
        conexion= Conexion.getConnection();
        System.err.println("me conecte");
        String sentencia_getAllOrdenTabla=("SELECT * FROM taqueria.detalle_venta");
        sentencia_preparada=conexion.prepareStatement(sentencia_getAllOrdenTabla);
        resultado=sentencia_preparada.executeQuery();
            modelo.addColumn("id_venta");
            modelo.addColumn("id_producto");
            modelo.addColumn("cantidad");
            modelo.addColumn("subtotal");
            modelo.addColumn("id_usuario");
            modelo.addColumn("id_mesa");
            OrdenTabla.setModel(modelo);
        while(resultado.next()){
            datos[0]=resultado.getString(1);
            datos[1]=resultado.getString(2);
            datos[2]=resultado.getString(3);
            datos[3]=resultado.getString(4);
            datos[4]=resultado.getString(5);
            datos[5]=resultado.getString(6);
            modelo.addRow(datos);
           OrdenTabla.setModel(modelo);
           /*System.out.println(datos[0]);
           System.out.println(datos[1]);
           System.out.println(datos[2]);*/
        }
        System.out.println("inserte los datos");
        }catch(Exception e){
            System.out.println(e);
        }
    }

                
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotonAgregarOrden;
    private javax.swing.JButton BotonMostrarProduc;
    private javax.swing.JButton BotonMostrarProduc1;
    private javax.swing.JButton BotonSalirOrden;
    private javax.swing.JButton BotonTicket;
    private javax.swing.JTextPane FechaOrden;
    private javax.swing.JLabel FechaOrdenLabel;
    private javax.swing.JLabel LabelDatosdelaVenta;
    private javax.swing.JButton MostrarOrden;
    private javax.swing.JTable OrdenTabla;
    private javax.swing.JLabel RegistrodeVentasLabel;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane8;
    private java.awt.ScrollPane scrollPane1;
    // End of variables declaration//GEN-END:variables
}
