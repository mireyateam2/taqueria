import java.sql.Connection;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import metodos.metodos_sql;
import static metodos.metodos_sql.Conexion;
import static metodos.metodos_sql.resultado;
import static metodos.metodos_sql.sentencia_preparada;


public class MenuProductos extends javax.swing.JFrame {
public String id;
    public MenuProductos() {
        initComponents();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton5 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        LabelMenuProductos = new javax.swing.JLabel();
        LabelProductosDisponibles = new javax.swing.JLabel();
        BotonAñadirProducto = new javax.swing.JButton();
        scrollPane1 = new java.awt.ScrollPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        TablaProductos = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        BotonModificarProduc = new javax.swing.JButton();
        BotonEliminarProduc = new javax.swing.JButton();
        BotonMostrarProduc = new javax.swing.JButton();
        BotonSalirProduc = new javax.swing.JButton();

        jButton5.setBackground(new java.awt.Color(173, 245, 191));
        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/2.png"))); // NOI18N
        jButton5.setText("Salir");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(198, 235, 249));

        LabelMenuProductos.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LabelMenuProductos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/LOGO.png"))); // NOI18N
        LabelMenuProductos.setText("Menu de Productos");

        LabelProductosDisponibles.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        LabelProductosDisponibles.setText("PRODUCTOS DISPONIBLES");

        BotonAñadirProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/añadir.png"))); // NOI18N
        BotonAñadirProducto.setText("Añadir nuevo producto");
        BotonAñadirProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonAñadirProductoActionPerformed(evt);
            }
        });

        scrollPane1.setBackground(new java.awt.Color(255, 255, 255));

        TablaProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(TablaProductos);

        scrollPane1.add(jScrollPane1);

        jPanel2.setBackground(new java.awt.Color(173, 245, 191));

        BotonModificarProduc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/MODIFICAR.png"))); // NOI18N
        BotonModificarProduc.setText("Modificar datos del Producto");
        BotonModificarProduc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonModificarProducActionPerformed(evt);
            }
        });

        BotonEliminarProduc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Limpiar.png"))); // NOI18N
        BotonEliminarProduc.setText("Eliminar Producto");
        BotonEliminarProduc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonEliminarProducActionPerformed(evt);
            }
        });

        BotonMostrarProduc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/BUSCAR.png"))); // NOI18N
        BotonMostrarProduc.setText("Mostrar productos");
        BotonMostrarProduc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonMostrarProducActionPerformed(evt);
            }
        });

        BotonSalirProduc.setBackground(new java.awt.Color(173, 245, 191));
        BotonSalirProduc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/2.png"))); // NOI18N
        BotonSalirProduc.setText("Salir");
        BotonSalirProduc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonSalirProducActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(BotonMostrarProduc)
                .addGap(18, 18, 18)
                .addComponent(BotonModificarProduc)
                .addGap(18, 18, 18)
                .addComponent(BotonEliminarProduc)
                .addGap(35, 35, 35)
                .addComponent(BotonSalirProduc, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(92, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BotonEliminarProduc)
                    .addComponent(BotonMostrarProduc)
                    .addComponent(BotonModificarProduc)
                    .addComponent(BotonSalirProduc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(LabelMenuProductos)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(62, 62, 62)
                        .addComponent(scrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 632, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(BotonAñadirProducto)
                        .addGap(72, 72, 72)
                        .addComponent(LabelProductosDisponibles)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(LabelMenuProductos)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addComponent(LabelProductosDisponibles))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(BotonAñadirProducto)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BotonSalirProducActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonSalirProducActionPerformed
        MenuPrincipal MP=new MenuPrincipal();
        MP.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonSalirProducActionPerformed

    private void BotonAñadirProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonAñadirProductoActionPerformed
        AñadirProductoNuevo AP=new AñadirProductoNuevo();
        AP.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonAñadirProductoActionPerformed

    private void BotonModificarProducActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonModificarProducActionPerformed
        id = TablaProductos.getValueAt(TablaProductos.getSelectedRow(),0).toString();
        ModificarProductos Mod=new ModificarProductos(id);
        Mod.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonModificarProducActionPerformed

    private void BotonMostrarProducActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonMostrarProducActionPerformed
        getAllProductos();
    }//GEN-LAST:event_BotonMostrarProducActionPerformed

    private void BotonEliminarProducActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonEliminarProducActionPerformed
        int res= JOptionPane.showConfirmDialog(null, "¿Seguro que deseas eliminar?");
       if(res==0){
        metodos_sql metodos= new metodos_sql();
        if (TablaProductos.getSelectedRow()==-1){
            JOptionPane.showMessageDialog(null, "Seleccione un producto primero");
        }else {
        if(metodos.eliminarProductos( TablaProductos.getValueAt(TablaProductos.getSelectedRow(), 0).toString())){
            System.out.println("Eliminado correctamente");
           BotonMostrarProduc.doClick();
        }
        else{
            JOptionPane.showMessageDialog(null, "Error al eliminar");
        }
        }
       }
       else {

       }
    }//GEN-LAST:event_BotonEliminarProducActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MenuProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MenuProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MenuProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MenuProductos().setVisible(true);
            }
        });
    }
    
    
    public void getAllProductos(){
        Connection conexion=null;
        String[] datos = new String[3];
        DefaultTableModel modelo = new DefaultTableModel();
        try {
        conexion= Conexion.getConnection();
        System.err.println("me conecte");
        String sentencia_getAllProductos=("SELECT * FROM taqueria.productos");
        sentencia_preparada=conexion.prepareStatement(sentencia_getAllProductos);
        resultado=sentencia_preparada.executeQuery();
            modelo.addColumn("id_prodcuto");
            modelo.addColumn("nombre_producto");
            modelo.addColumn("costo_producto");
            TablaProductos.setModel(modelo);
        while(resultado.next()){
            datos[0]=resultado.getString(1);
            datos[1]=resultado.getString(2);
            datos[2]=resultado.getString(3);
            modelo.addRow(datos);
           TablaProductos.setModel(modelo);
           /*System.out.println(datos[0]);
           System.out.println(datos[1]);
           System.out.println(datos[2]);*/
        }
        System.out.println("inserte los datos");
        }catch(Exception e){
            System.out.println(e);
        }
        
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotonAñadirProducto;
    private javax.swing.JButton BotonEliminarProduc;
    private javax.swing.JButton BotonModificarProduc;
    private javax.swing.JButton BotonMostrarProduc;
    private javax.swing.JButton BotonSalirProduc;
    private javax.swing.JLabel LabelMenuProductos;
    private javax.swing.JLabel LabelProductosDisponibles;
    private javax.swing.JTable TablaProductos;
    private javax.swing.JButton jButton5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private java.awt.ScrollPane scrollPane1;
    // End of variables declaration//GEN-END:variables
}
