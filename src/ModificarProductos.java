import java.util.Date;
import javax.swing.JOptionPane;
import metodos.metodos_sql;

public class ModificarProductos extends javax.swing.JFrame {
String id;

    public ModificarProductos(String id) {
        initComponents();
        setLocationRelativeTo(null);
        Date fecha=new Date();
        FechaModificarProduc.enable(false);
        String f = fecha.toGMTString().substring(0, 12);
        FechaModificarProduc.setText(f);
        this.id=id;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        LabelModificarProducto = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        textNombreProduc = new javax.swing.JTextPane();
        LabelNombreProduc = new javax.swing.JLabel();
        LabelPrecio = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        textPrecioProduc = new javax.swing.JTextPane();
        jPanel2 = new javax.swing.JPanel();
        ActualizarProduc = new javax.swing.JButton();
        CancelarProduc = new javax.swing.JButton();
        LimpiarProduc = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        FechaModificarProduc = new javax.swing.JTextPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(198, 235, 249));

        LabelModificarProducto.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        LabelModificarProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/LOGO.png"))); // NOI18N
        LabelModificarProducto.setText("Modificar Producto");

        textNombreProduc.setBackground(new java.awt.Color(173, 245, 191));
        jScrollPane1.setViewportView(textNombreProduc);

        LabelNombreProduc.setText("Nombre:");

        LabelPrecio.setText("Costo o precio:");

        textPrecioProduc.setBackground(new java.awt.Color(173, 245, 191));
        jScrollPane2.setViewportView(textPrecioProduc);

        jPanel2.setBackground(new java.awt.Color(173, 245, 191));

        ActualizarProduc.setText("Actualizar");
        ActualizarProduc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ActualizarProducActionPerformed(evt);
            }
        });

        CancelarProduc.setText("Cancelar");
        CancelarProduc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CancelarProducActionPerformed(evt);
            }
        });

        LimpiarProduc.setText("Limpiar");
        LimpiarProduc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LimpiarProducActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addComponent(ActualizarProduc)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(LimpiarProduc)
                .addGap(59, 59, 59)
                .addComponent(CancelarProduc)
                .addGap(60, 60, 60))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(ActualizarProduc)
                .addComponent(CancelarProduc)
                .addComponent(LimpiarProduc))
        );

        FechaModificarProduc.setEnabled(false);
        jScrollPane6.setViewportView(FechaModificarProduc);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(LabelPrecio)
                    .addComponent(LabelNombreProduc))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 49, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(LabelModificarProducto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(LabelModificarProducto)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(LabelNombreProduc))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(LabelPrecio)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ActualizarProducActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ActualizarProducActionPerformed
        metodos_sql metodos=new metodos_sql();

        if (textNombreProduc.getText().isEmpty()|| textPrecioProduc.getText().isEmpty()){
            JOptionPane.showMessageDialog (null, "No puede quedar NINGUN campo vacio");
        }
        else {
            metodos.editarProductos(id,textNombreProduc.getText(), textPrecioProduc.getText());
            JOptionPane.showMessageDialog (null, "Producto actualizado con exito");
            textNombreProduc.setText("");
            textPrecioProduc.setText("");
        }
    }//GEN-LAST:event_ActualizarProducActionPerformed

    private void CancelarProducActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CancelarProducActionPerformed
        MenuProductos a=new MenuProductos();
        a.show(true);
        this.dispose();

    }//GEN-LAST:event_CancelarProducActionPerformed

    private void LimpiarProducActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LimpiarProducActionPerformed
        limpiar();
    }//GEN-LAST:event_LimpiarProducActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ModificarProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ModificarProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ModificarProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ModificarProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
          
            }
        });
    }
    
    void limpiar (){
        textNombreProduc.setText("");
        textPrecioProduc.setText("");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ActualizarProduc;
    private javax.swing.JButton CancelarProduc;
    private javax.swing.JTextPane FechaModificarProduc;
    private javax.swing.JLabel LabelModificarProducto;
    private javax.swing.JLabel LabelNombreProduc;
    private javax.swing.JLabel LabelPrecio;
    private javax.swing.JButton LimpiarProduc;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTextPane textNombreProduc;
    private javax.swing.JTextPane textPrecioProduc;
    // End of variables declaration//GEN-END:variables
}
