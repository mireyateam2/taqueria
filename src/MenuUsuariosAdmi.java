import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import jdk.nashorn.internal.scripts.JO;
import metodos.conectar;
import metodos.metodos_sql;
import static metodos.metodos_sql.Conexion;
import static metodos.metodos_sql.resultado;
import static metodos.metodos_sql.sentencia_preparada;


public class MenuUsuariosAdmi extends javax.swing.JFrame {
    
    public String nombre;
    public String id;

    public MenuUsuariosAdmi() {
        initComponents();
        setLocationRelativeTo(null);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        LabelMenuAdministrador = new javax.swing.JLabel();
        BotonAñadirUsuario = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        BotonMostrarUsuarios = new javax.swing.JButton();
        BotonModificarUsuario = new javax.swing.JButton();
        BotonEliminarUsuario = new javax.swing.JButton();
        BotonSalirUsuario = new javax.swing.JButton();
        LabelUsuariosDisponibles = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        TablaUsuarios = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(198, 235, 249));

        LabelMenuAdministrador.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LabelMenuAdministrador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/LOGO.png"))); // NOI18N
        LabelMenuAdministrador.setText("Menu de Administrador");

        BotonAñadirUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/añadir.png"))); // NOI18N
        BotonAñadirUsuario.setText("Añadir nuevo usuario");
        BotonAñadirUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonAñadirUsuarioActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(173, 245, 191));
        jPanel2.setRequestFocusEnabled(false);

        BotonMostrarUsuarios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/BUSCAR.png"))); // NOI18N
        BotonMostrarUsuarios.setText("Mostrar usuarios");
        BotonMostrarUsuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonMostrarUsuariosActionPerformed(evt);
            }
        });

        BotonModificarUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/CambiarUsuario.png"))); // NOI18N
        BotonModificarUsuario.setText("Modificar datos del usuario");
        BotonModificarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonModificarUsuarioActionPerformed(evt);
            }
        });

        BotonEliminarUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Limpiar.png"))); // NOI18N
        BotonEliminarUsuario.setText("Eliminar usuario");
        BotonEliminarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonEliminarUsuarioActionPerformed(evt);
            }
        });

        BotonSalirUsuario.setBackground(new java.awt.Color(173, 245, 191));
        BotonSalirUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/2.png"))); // NOI18N
        BotonSalirUsuario.setText("Salir");
        BotonSalirUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonSalirUsuarioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(BotonMostrarUsuarios, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BotonModificarUsuario)
                .addGap(10, 10, 10)
                .addComponent(BotonEliminarUsuario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BotonSalirUsuario)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BotonEliminarUsuario)
                    .addComponent(BotonMostrarUsuarios)
                    .addComponent(BotonModificarUsuario)
                    .addComponent(BotonSalirUsuario))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        LabelUsuariosDisponibles.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        LabelUsuariosDisponibles.setText("Usuarios Diponibles");

        jScrollPane2.setRequestFocusEnabled(false);

        TablaUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(TablaUsuarios);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LabelMenuAdministrador)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(BotonAñadirUsuario))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(67, 67, 67)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 528, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(227, 227, 227)
                        .addComponent(LabelUsuariosDisponibles)))
                .addContainerGap(55, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(LabelMenuAdministrador)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BotonAñadirUsuario)
                .addGap(11, 11, 11)
                .addComponent(LabelUsuariosDisponibles)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE)
                .addGap(29, 29, 29)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.getAccessibleContext().setAccessibleName("");
        jPanel2.getAccessibleContext().setAccessibleDescription("");
        jScrollPane2.getAccessibleContext().setAccessibleName("");
        jScrollPane2.getAccessibleContext().setAccessibleDescription("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BotonSalirUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonSalirUsuarioActionPerformed
        MenuPrincipal MP=new MenuPrincipal();
        MP.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonSalirUsuarioActionPerformed

   
    
    private void BotonModificarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonModificarUsuarioActionPerformed
         id = TablaUsuarios.getValueAt(TablaUsuarios.getSelectedRow(), 0).toString();
          nombre= TablaUsuarios.getValueAt(TablaUsuarios.getSelectedRow(), 1).toString();
        
        ModificarUsuario u=new ModificarUsuario(id, nombre);
        u.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonModificarUsuarioActionPerformed

    private void BotonMostrarUsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonMostrarUsuariosActionPerformed
        getAllUsers();
    }//GEN-LAST:event_BotonMostrarUsuariosActionPerformed

    private void BotonAñadirUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonAñadirUsuarioActionPerformed
        AñadorUsuarioNuevo AU=new AñadorUsuarioNuevo();
        AU.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonAñadirUsuarioActionPerformed

    private void BotonEliminarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonEliminarUsuarioActionPerformed
       int res= JOptionPane.showConfirmDialog(null, "Seguro que deseas eliminar?");
       if(res==0){
        metodos_sql metodos= new metodos_sql();
        if (TablaUsuarios.getSelectedRow()==-1){
            JOptionPane.showMessageDialog(null, "Seleccione un usuario primero");
        }else {
        if(metodos.eliminarUsuario( TablaUsuarios.getValueAt(TablaUsuarios.getSelectedRow(), 0).toString())){
            System.out.println("Eliminado correctamente");
           BotonMostrarUsuarios.doClick();
        }
        else{
            JOptionPane.showMessageDialog(null, "Error al eliminar");
        }
        }
       }
       else {

       }
    }//GEN-LAST:event_BotonEliminarUsuarioActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MenuUsuariosAdmi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MenuUsuariosAdmi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MenuUsuariosAdmi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuUsuariosAdmi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MenuUsuariosAdmi().setVisible(true);
            }
        });
    }
    
        public void getAllUsers(){
        Connection conexion=null;
        String[] datos = new String[3];
        DefaultTableModel modelo = new DefaultTableModel();
        try {
        conexion= Conexion.getConnection();
        System.err.println("me conecte");
        String sentencia_getAllUsers=("SELECT * FROM taqueria.users");
        sentencia_preparada=conexion.prepareStatement(sentencia_getAllUsers);
        resultado=sentencia_preparada.executeQuery();
            modelo.addColumn("id_usuario");
            modelo.addColumn("nombre");
            modelo.addColumn("fecha_registro");
            TablaUsuarios.setModel(modelo);
        while(resultado.next()){
            datos[0]=resultado.getString(1);
            datos[1]=resultado.getString(2);
            datos[2]=resultado.getString(4);
            modelo.addRow(datos);
           TablaUsuarios.setModel(modelo);
           /*System.out.println(datos[0]);
           System.out.println(datos[1]);
           System.out.println(datos[2]);*/
        }
        System.out.println("inserte los datos");
        }catch(Exception e){
            System.out.println(e);
        }
    }
            private void EliminarUsers() {
                
                int fila = TablaUsuarios.getSelectedRow();
                String valor=TablaUsuarios.getValueAt(fila, 0).toString();
                if(fila>=0){
                String sentencia_EliminarUsers=("SELECT * FROM taqueria.users");
                }
            }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotonAñadirUsuario;
    private javax.swing.JButton BotonEliminarUsuario;
    private javax.swing.JButton BotonModificarUsuario;
    private javax.swing.JButton BotonMostrarUsuarios;
    private javax.swing.JButton BotonSalirUsuario;
    private javax.swing.JLabel LabelMenuAdministrador;
    private javax.swing.JLabel LabelUsuariosDisponibles;
    private javax.swing.JTable TablaUsuarios;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables


}
