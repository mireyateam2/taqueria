import java.util.Date;
import javax.swing.JOptionPane;
import metodos.metodos_sql;
public class AñadirMesaNueva extends javax.swing.JFrame {
public String f="";
    public AñadirMesaNueva() {
        initComponents();
        setLocationRelativeTo(null);
        Date fecha=new Date();
        FechaMesa.enable(false);
        String f = fecha.toGMTString().substring(0, 12);
        FechaMesa.setText(f);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane6 = new javax.swing.JScrollPane();
        JFecha1 = new javax.swing.JTextPane();
        BotonLimpiar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        AñadirMesa = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        BotonGuardarMesa = new javax.swing.JButton();
        BotonLimpiar1 = new javax.swing.JButton();
        BotonCancelarMesas = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        textNumero = new javax.swing.JTextPane();
        NumeroLabel = new javax.swing.JLabel();
        CapacidadLabel = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        textCapacidad = new javax.swing.JTextPane();
        jScrollPane7 = new javax.swing.JScrollPane();
        FechaMesa = new javax.swing.JTextPane();

        JFecha1.setEnabled(false);
        jScrollPane6.setViewportView(JFecha1);

        BotonLimpiar.setText("Limpiar");
        BotonLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonLimpiarActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(198, 235, 249));

        AñadirMesa.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        AñadirMesa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/LOGO.png"))); // NOI18N
        AñadirMesa.setText("Añadir Mesa");

        jPanel2.setBackground(new java.awt.Color(173, 245, 191));

        BotonGuardarMesa.setText("Guardar");
        BotonGuardarMesa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonGuardarMesaActionPerformed(evt);
            }
        });

        BotonLimpiar1.setText("Limpiar");
        BotonLimpiar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonLimpiar1ActionPerformed(evt);
            }
        });

        BotonCancelarMesas.setText("Cancelar");
        BotonCancelarMesas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonCancelarMesasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addComponent(BotonGuardarMesa)
                .addGap(55, 55, 55)
                .addComponent(BotonLimpiar1)
                .addGap(55, 55, 55)
                .addComponent(BotonCancelarMesas)
                .addContainerGap(76, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(0, 11, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BotonLimpiar1)
                    .addComponent(BotonGuardarMesa)
                    .addComponent(BotonCancelarMesas))
                .addContainerGap())
        );

        textNumero.setBackground(new java.awt.Color(173, 245, 191));
        jScrollPane1.setViewportView(textNumero);

        NumeroLabel.setText("Numero:");

        CapacidadLabel.setText("Capacidad:");

        textCapacidad.setBackground(new java.awt.Color(173, 245, 191));
        jScrollPane2.setViewportView(textCapacidad);

        FechaMesa.setEnabled(false);
        jScrollPane7.setViewportView(FechaMesa);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(AñadirMesa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(CapacidadLabel)
                    .addComponent(NumeroLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING))
                .addGap(73, 73, 73))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(AñadirMesa)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(NumeroLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CapacidadLabel))
                .addGap(38, 38, 38)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BotonGuardarMesaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonGuardarMesaActionPerformed
        metodos_sql metodos = new metodos_sql();

        if (textNumero.getText().isEmpty()|| textCapacidad.getText().isEmpty()){
            JOptionPane.showMessageDialog (null, "No puede quedar NINGUN campo vacio");
        }
        else if(buscarMesasRegistradas(textNumero.getText(),textCapacidad.getText())){
            JOptionPane.showMessageDialog (null, "Numero de mesa YA REGISTRADO");
        }
        else{
            metodos.guardarMesa(textNumero.getText() ,textCapacidad.getText());
            JOptionPane.showMessageDialog (null, "Mesa guardada correctamente");
            textNumero.setText("");
            textCapacidad.setText("");
        }
    }//GEN-LAST:event_BotonGuardarMesaActionPerformed

    private void BotonLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonLimpiarActionPerformed
        
    }//GEN-LAST:event_BotonLimpiarActionPerformed

    private void BotonLimpiar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonLimpiar1ActionPerformed
        limpiar();
    }//GEN-LAST:event_BotonLimpiar1ActionPerformed

    private void BotonCancelarMesasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonCancelarMesasActionPerformed
        MenuMesas MT=new MenuMesas();
        MT.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonCancelarMesasActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AñadirMesaNueva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AñadirMesaNueva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AñadirMesaNueva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AñadirMesaNueva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AñadirMesaNueva().setVisible(true);
            }
        });
    }
    
    void limpiar (){
            textNumero.setText("");
            textCapacidad.setText("");
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel AñadirMesa;
    private javax.swing.JButton BotonCancelarMesas;
    private javax.swing.JButton BotonGuardarMesa;
    private javax.swing.JButton BotonLimpiar;
    private javax.swing.JButton BotonLimpiar1;
    private javax.swing.JLabel CapacidadLabel;
    private javax.swing.JTextPane FechaMesa;
    private javax.swing.JTextPane JFecha1;
    private javax.swing.JLabel NumeroLabel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JTextPane textCapacidad;
    private javax.swing.JTextPane textNumero;
    // End of variables declaration//GEN-END:variables

    private boolean buscarMesasRegistradas(String text, String text0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
