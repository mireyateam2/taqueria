
import java.util.Date;
import javax.swing.JOptionPane;
import metodos.metodos_sql;

public class ModificarTaqueria extends javax.swing.JFrame {

    public ModificarTaqueria() {
        initComponents();
        setLocationRelativeTo(null);
        Date fecha=new Date();
        FechaTaqueria.enable(false);
        String f = fecha.toGMTString().substring(0, 12);
        FechaTaqueria.setText(f);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        LabelModificarTaqueria = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        BotonActualizarTaqueria = new javax.swing.JButton();
        BotonCancelarTaqueria = new javax.swing.JButton();
        BotonLimpiar1 = new javax.swing.JButton();
        NombreComplejoLabel = new javax.swing.JLabel();
        TelefonoTaqueriaLabel = new javax.swing.JLabel();
        DireccionLabel = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        textNombreComplejo = new javax.swing.JTextPane();
        jScrollPane3 = new javax.swing.JScrollPane();
        textDireccion = new javax.swing.JTextPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        textTelefono = new javax.swing.JTextPane();
        jScrollPane6 = new javax.swing.JScrollPane();
        FechaTaqueria = new javax.swing.JTextPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(198, 235, 249));

        LabelModificarTaqueria.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        LabelModificarTaqueria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/LOGO.png"))); // NOI18N
        LabelModificarTaqueria.setText("Modificar Taquería");

        jPanel2.setBackground(new java.awt.Color(173, 245, 191));

        BotonActualizarTaqueria.setText("Actualizar");
        BotonActualizarTaqueria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonActualizarTaqueriaActionPerformed(evt);
            }
        });

        BotonCancelarTaqueria.setText("Cancelar");
        BotonCancelarTaqueria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonCancelarTaqueriaActionPerformed(evt);
            }
        });

        BotonLimpiar1.setText("Limpiar");
        BotonLimpiar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonLimpiar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addComponent(BotonActualizarTaqueria)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                .addComponent(BotonLimpiar1)
                .addGap(57, 57, 57)
                .addComponent(BotonCancelarTaqueria)
                .addGap(48, 48, 48))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(BotonActualizarTaqueria)
                .addComponent(BotonCancelarTaqueria)
                .addComponent(BotonLimpiar1))
        );

        NombreComplejoLabel.setText("Nombre del complejo:");

        TelefonoTaqueriaLabel.setText("Telefono:");

        DireccionLabel.setText("Dirección:");

        textNombreComplejo.setBackground(new java.awt.Color(173, 245, 191));
        jScrollPane2.setViewportView(textNombreComplejo);

        textDireccion.setBackground(new java.awt.Color(173, 245, 191));
        jScrollPane3.setViewportView(textDireccion);

        textTelefono.setBackground(new java.awt.Color(173, 245, 191));
        jScrollPane4.setViewportView(textTelefono);

        FechaTaqueria.setEnabled(false);
        jScrollPane6.setViewportView(FechaTaqueria);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(DireccionLabel)
                    .addComponent(NombreComplejoLabel)
                    .addComponent(TelefonoTaqueriaLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                    .addComponent(jScrollPane4)
                    .addComponent(jScrollPane3))
                .addGap(0, 65, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(LabelModificarTaqueria)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(LabelModificarTaqueria)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(NombreComplejoLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TelefonoTaqueriaLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DireccionLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BotonActualizarTaqueriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonActualizarTaqueriaActionPerformed
        metodos_sql metodos=new metodos_sql();
         
         
          if (textNombreComplejo.getText().isEmpty()||textTelefono.getText().isEmpty()||textDireccion.getText().isEmpty()){
            JOptionPane.showMessageDialog (null, "No puede quedar NINGUN campo vacio");
        }
        else {
         metodos.editarTaqueria(textNombreComplejo.getText(),textTelefono.getText(), textDireccion.getText());
         JOptionPane.showMessageDialog (null, "Usuario actualizado con exito");
            textNombreComplejo.setText("");
            textTelefono.setText("");
            textDireccion.setText("");
        }
    }//GEN-LAST:event_BotonActualizarTaqueriaActionPerformed

    private void BotonCancelarTaqueriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonCancelarTaqueriaActionPerformed
        MenuTaqueria MT=new MenuTaqueria();
        MT.show(true);
        this.dispose();
    }//GEN-LAST:event_BotonCancelarTaqueriaActionPerformed

    private void BotonLimpiar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonLimpiar1ActionPerformed
        limpiar();
    }//GEN-LAST:event_BotonLimpiar1ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ModificarTaqueria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ModificarTaqueria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ModificarTaqueria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ModificarTaqueria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ModificarTaqueria().setVisible(true);
            }
        });
    }
    
    void limpiar (){
        textNombreComplejo.setText("");
        textTelefono.setText("");
        textDireccion.setText("");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotonActualizarTaqueria;
    private javax.swing.JButton BotonCancelarTaqueria;
    private javax.swing.JButton BotonLimpiar1;
    private javax.swing.JLabel DireccionLabel;
    private javax.swing.JTextPane FechaTaqueria;
    private javax.swing.JLabel LabelModificarTaqueria;
    private javax.swing.JLabel NombreComplejoLabel;
    private javax.swing.JLabel TelefonoTaqueriaLabel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTextPane textDireccion;
    private javax.swing.JTextPane textNombreComplejo;
    private javax.swing.JTextPane textTelefono;
    // End of variables declaration//GEN-END:variables
}
